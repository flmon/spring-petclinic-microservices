# DemarcheDevOpsOpenClassroom

- Le cours Openclassroom se trouve à l'adresse
  https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6182806-planifiez-votre-developpement

Il utilise Gitlab (https://gitlab.com/users/sign_in)

###Section A: planification du developpement

- Creez un nouveau projet dan Gitlab
- Aller sur Project Information, Labels
- Creer Labels To Do et Doing ainsi que d'autres t.q Epic,
  User story...
- Aller sur Issues, Boards; creer listes To Do, Doing et In review
- Creer nouveau board (Product) et y mettre les listes Low, Medium, High, Rejected
  Ce board representera le Product Backlog
- Creer une epic (un ensemble de user stories) en faisant en fait une issue.
  dans List, nouvelle Issue en mettant Epic comme label
- Repeter avec une user story
- Lier la user story a l'epic (Linked issues, ajouter numero p.ex. #2)
- On peut deplacer (dragAndDrop) les issues dans les boards p.ex. deplacer la user story de Open vers To Do (list)
  ou deplacer l'epic dans Medium (l'epic est liee au Product Backlog alors que la user story reste dans Board Development)
  avec Low, Medium, High, on peut prioriser les epic ou avec To Do,... on peut visualiser l'avancement des user stories
- Creer un Milestone (qui symbolisera un sprint)
- Sur l'issue user story, on peut modifier des parametres pour lier la milestone (sprint), assigner qqn,
  faire du time tracking (avec spend/estimate : /spend 2d   /estimate 1w)


